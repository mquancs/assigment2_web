    <?php
        session_start();
        include_once("../Model/transaction.php");
        include_once("../Model/order.php");

        $model = new transaction();
        $order = new order();
        
        if(isset($_POST)){
            if($_SERVER['REQUEST_METHOD'] == "POST"){
                
                $totalPrice = $_POST["totalPrice"];
                $transactionID =$model->addTransaction( $_SESSION['userid'],$totalPrice);
                $productId = '';
                $quantity = '';
                $price = '';
                $i = 1;
                foreach( $_POST as $key => $value ){
                    if(substr($key, 0, 5) == 'produ'){
                        $productId = $_POST[$key];
                    }
                    else if(substr($key, 0, 5) == 'quant'){
                        $quantity = $_POST[$key];
                    }
                    else if(substr($key, 0, 5) == 'price'){
                        $price = $_POST[$key];
                    }
                    
                    if( $i % 3 == 0){
                        $order->addOrder( $transactionID, $_SESSION['userid'],$productId,$quantity,$price);
                    } 
                    $i++;
                }
                foreach ($_SESSION as $name => $value) {
                    if(substr($name, 0, 5) == 'cart_'){
                        unset($_SESSION[$name]);
                    }
                }
                $_SESSION['sucMsg'] = "Giao dịch thành công";
                header("Location: ../Views/order-detail.php");
                // $productId = $_POST["productId"];
                // var_dump($transactionID);
                // $price = $_POST["price"];
                // $quantity = $_POST['quantity'];

                // for($i = 0; $i <= count($productId); $i++){
                //     $order->addOrder( $transactionID, $_SESSION['userid'],$productId[$i],$quantity[$i],$price[$i]);
                // }
                
                
            }
        }
    ?>
