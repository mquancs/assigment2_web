<?php
	
	require ('../Model/BlogModel.php');

	class BlogController
	{

		protected $util, $model;
		private $id;

		public function __construct() {

			// if(empty($_SESSION)) {
			// 	@session_start();
			// }

		}

		/*public function index()
	    {
	        $this->oUtil->oPosts = $this->oModel->get(0, 5); // Get only the latest 5 posts

	        $this->oUtil->getView('index');

	    }

	    public function post()
	    {
	        
	    }

	    public function notFound()
	    {
	        
	    }*/


	    /***** For Admin (Back end) *****/
	    /*public function all()
	    {
	        
	    }*/
	    public function getAll() {
	    	$gModel = new BlogModel;
	    	$result = $gModel->getAll();
	    	return $result;
	    }

	    public function getById($id) {
	    	$gModel = new BlogModel;
	    	$result = $gModel->getById($id);
	    	return $result;
	    }

	    public function add($sTitle, $sContent, $sAuthor, $sAmountOfViews, $sPostingDate, $sImage)
	    {
	    	echo("BlogController->add start");

	       	$aModel = new BlogModel();

	       	echo('__PHP__: new blog');

	   		

   			$aModel->title = $sTitle;
   			$aModel->content = $sContent;
   			$aModel->author = $sAuthor;
   			$aModel->amountOfViews = $sAmountOfViews;
   			$aModel->postingDate = $sPostingDate ;//date("d-m-Y");
   			$aModel->image = $sImage;

   			echo('__PHP__: Insert title and content');	

   			if(!$aModel->insert()){
   				echo("<script>console.log('__PHP__: Insert data FAIL');</script>");	
   			} else {
   				echo("<script>console.log('__PHP__: Insert data Success');</script>");
   			}
	   		
	    }

	    public function edit($sId, $sTitle, $sContent, $sImage)
	    {
	        $eModel = new BlogModel;

        	$eModel->title = $sTitle;
       		$eModel->content = $sContent;
       		$eModel->id = $sId;
       		$eModel->image = $sImage;
        	
        	echo "<script>console.log('__PHP__Controller: before update');</script>";

       		if(!$eModel->update()) {
       			echo "<script>console.log('__PHP__Controller: Update data FAIL');</script>";
	        } 

	    }

	    public function delete($sId)
	    {
	        //if(!$this->isLogged()) exit;

	        $dModel = new BlogModel;

	        $dModel->id = $sId;

	        echo "<script>console.log('__PHP__Controller: before delete');</script>";
	        if(!$dModel->delete()) {
	        	echo("<script>console.log('__PHP__: Delete data FAIL')</script>");
	        }
	        
	    }

	    protected function isLogged()
	    {
	        return !empty($_SESSION['is_logged']);
	    }
	}
?>