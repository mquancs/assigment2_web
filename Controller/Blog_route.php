<?php

	require_once('BlogController.php');
	
	if(isset($_POST["add_submit"])) {

		echo("<script>console.log('__PHP__: route start');</script>");

		if(isset($_POST['title'], $_POST['body'], $_POST['image'])) {
 			echo("<script>console.log('__PHP__:isset title body');</script>");
			$blog_controller = new BlogController;
			$blog_controller->add($_POST['title'], $_POST['body'], '', 0, date("Y-m-d"), $_POST['image']);
			echo("<script>console.log('__PHP__: route success, time".date("d-m-Y")."');</script>");
		}
		else {
			echo("<script>console.log('__PHP__:isset title body FAIL');</script>");
		}
		header('Location: ../Views/blogMain.php');
	}

	if(isset($_POST["edit_submit"])) {

		echo("<script>console.log('__PHP__: route start');</script>");

		if(isset($_POST['title'], $_POST['body'], $_POST['image'], $_POST['id'])) {
 			//echo("<script>console.log('__PHP__:isset title body:".$_POST['title'].$_POST['body'].$_POST['image'].$_POST['id']."');</script>");
			$blog_controller_editer = new BlogController;
			//echo("<script>console.log('__PHP__:isset title body: new BlogController');</script>");
			$blog_controller_editer->edit($_POST['id'], $_POST['title'], $_POST['body'], $_POST['image']);
			//echo("<script>console.log('__PHP__: route success');</script>");
		}
		else {
			echo("<script>console.log('__PHP__:isset title body FAIL');</script>");
		}

		header('Location: ../Views/blogMain.php');
	}
	
	if(isset($_GET["action"])) {
		echo("<script>console.log('__PHP__: route start action');</script>");
		if(isset($_GET["id"])) {
			echo("<script>console.log('__PHP__: route start');</script>");
			$blog_controller_editer = new BlogController;
			//echo("<script>console.log('__PHP__:isset title body: new BlogController');</script>");
			$blog_controller_editer->delete($_GET["id"]);
			echo("<script>console.log('__PHP__: route success');</script>");
		}

		header('Location: ../Views/blogMain.php');
	}
	
?>