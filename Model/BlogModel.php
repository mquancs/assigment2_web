<?php

	require_once('../Utilities/Db.php');
	
	class BlogModel 
	{
		
		public $id;
		public $title;
		public $content;
		public $amountOfViews;
		public $postingDate;
		public $author;
		public $image;
			
		public function __construct()
		{

		}
		
		public function getBySql($sql) 
		{
			$db = new Db;
			
			$result = $db->query($sql);
			
			$objects = array();
			
			while($object = $result->fetch_object()) {
				$objects[] = $object;
			}
			
			$db->close();
			
			return $objects;
		}
		
		public static function getAll()
		{
			$qry = 'select * from POSTS ORDER BY PostingDate DESC ';
			
			$db = new Db;
			
			$result = $db->query($qry);

			/*if($result) 
			{
				while($row = $result->fetch_assoc()) 
				{
					echo "Db_TITLE_POST".$row['Title']."";
				}
			}*/
			return $result;	
		}
		
		public static function getById($id)
		{
			$qry = 'select * from POSTS where ID = ?';
			
			$db = new Db;
			
			// get instance of statement
			$statement = $db->stmt_init();
			
			//Prepare query
			if($statement->prepare($qry)) {
				
				//Bind paramenters
				$statement->bind_param('i', $id);
				
				//Execute statement
				$statement->execute();

				//Bind variable to prepared statement
				$statement->bind_result($id, $title, $content, $amountOfViews, $postingDate, $author, $image);

				//Populate bind variables
				$statement->fetch();

				//Close statement
				$statement->close();
			}

			$db->close();

			$object = new self;
			$object->id = $id;
			$object->title = $title;
			$object->content = $content;
			$object->amountOfViews = $amountOfViews;
			$object->postingDate = $postingDate;
			$object->author = $author;
			$object->image = $image;

			return $object;
		}
		
		public function insert()
		{
			$affected_rows = false;

			$qry = "insert into POSTS (title, Content, AmountOfViews, PostingDate, Author, Images) values(?,?,?,?,?,?)";

			$db = new Db;

			$statement = $db->stmt_init();

			if($statement->prepare($qry)) 
			{
				$statement->bind_param('ssisss', $this->title, $this->content, $this->amountOfViews, $this->postingDate, $this->author, $this->image);

				$statement->execute();

				$affected_rows = $db->affected_rows;

				$statement->close();
			}

			$db->close();

			header("location: ../Views/blogAddPost.php");	

			return $affected_rows;

		}

		public function update() {

			$affected_rows = FALSE;

			echo "<script>console.log('__PHP__Model: start update');</script>";

			$qry = "update POSTS set title = ?, content = ?, images = ? where id = ?";

			$db = new Db;

			$statement = $db->stmt_init();

			if($statement->prepare($qry)) {

				$statement->bind_param('sssi', $this->title, $this->content, $this->image, $this->id);

				$statement->execute();

				$affected_rows = $db->affected_rows;

				$statement->close();
			}

			echo "<script>console.log('__PHP__Model: ".$qry."');</script>";
			$db->close();

			return $affected_rows;
		}

		public function delete() {

			$affected_rows = FALSE;

			$qry = "delete from POSTS where id = ?";

			$db = new Db;

			$statement = $db->stmt_init();

			if($statement->prepare($qry)) 
			{
				$statement->bind_param('i', $this->id);

				$statement->execute();

				$affected_rows = $db->affected_rows;

				$statement->close();
			}

			$db->close();

			return $affected_rows;
		}
		
	}

?>