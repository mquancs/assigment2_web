<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="http://localhost/ass2/">HOME</a></li>
                    <li class="dropdown">
                        <a href="http://localhost/ass2/?id_catalog=2">ĐỒ DÙNG HỌC TẬP<span></span></a>
                        <!-- <ul class="dropdown-menu">
                        class="dropdown-toggle" data-toggle="dropdown"
                            <li><a href="#">Bút</a></li>
                            <li><a href="#">Sách giáo khoa</a></li>
                            <li><a href="#">Tập viết</a></li>
                        </ul> -->
                    </li>
                    <li><a href="http://localhost/ass2/?id_catalog=3">QUÀ TẶNG VALENTINE</a></li>
                    <li><a href="http://localhost/ass2/?id_catalog=4">SẢN PHẨM HOT</a></li>
                    <li><a href="http://localhost/ass2/Views/blogMain.php">BLOG</a></li>
                    <li><a href="http://localhost/ass2/Views/contactUs.php">LIÊN HỆ</a></li>
                    
                </ul>
                <ul class="nav navbar-nav navbar-right">
                <?php if(!isset($_SESSION['username'])){ ?>
                    <li><a href="http://localhost/ass2/Views/signUp.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="http://localhost/ass2/Views/login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                <?php  }else {?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"> Xin chào <?php echo $_SESSION['username'] ?><span
                                class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="Views/changeInfor.php"><span class="glyphicon glyphicon-user"></span> Thay đổi thông tin</a></li>
                            <li><a href="Views/changePassword.php"><span class="glyphicon glyphicon-user"></span> Thay đổi mật khẩu</a></li>
                            <li><a href="Controller/logout.php"><span class="glyphicon glyphicon-user"></span> Logout</a></li>
                        </ul>
                    </li>
                    
                <?php }?>
                </ul>
            </div>
        </div>
    </nav>