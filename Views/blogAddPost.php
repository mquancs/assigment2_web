<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Blog</title>
	
	<link rel="stylesheet" type="text/css" href="../style.css">
	<link rel="stylesheet" type="text/css" href="../css/styleHome.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	
    <?php
        include('header.php');
        include('navUser.php');
    ?>
    
	<form action="..\Controller\Blog_route.php" method="post">
		<div class="col-md-12">

		    <div class="form-group">
		    	<label for="title">Title:</label>
			    <input type="text" class="form-control" name="title" id="title" required="required" placeholder="Title...">
			</div>

		    <div class="form-group">
			  	<label for="body">Body:</label>
			   	<textarea class="form-control" name="body" id="body" rows="5" cols="35" required="required"></textarea>
			</div>

		    <div class="form-group">
		    	<label for="image">Image URL:</label><br />
		        <input type="text" class="form-control" name="image" id="image" required="required"  placeholder="URL..."/>
		    </div>

		    <div style="margin-bottom: 70px">
		    	<input type="submit" class="admin-button btn btn-success" name="add_submit" value="Post"/>
			</div>

		</div>
	</form>
    
    <?php include('footer.php');?>
    
</body>
</html>