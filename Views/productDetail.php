<!DOCTYPE HTML>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Details</title>
	
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/styleHome.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/style_intro.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<script type="text/javascript">
		changeImage = function(el, id, url, width, height, title, alt) {
			var d = document, target = d.getElementById(id);
			if(!target) return false;
			target.src = url;
			target.width = width;
			target.height = height;
			target.title = title;
			target.alt = alt;
			return false;
		};
	</script>
</head>
<body>
<?php
include("$_SERVER[DOCUMENT_ROOT]/ass2/Controller/displayProduct.php");
$id = $_GET['id'];
$result = mysql_query("SELECT * from products WHERE product_ID='$id'");

?>
	<?php
		include('header.php');
		include('navUser.php');
	?>
	<?php 
		while($dong_sp=mysql_fetch_array($result)) { 
	?>
	<div class="detail">
		<div class="row-img">
			<div class="col-md-12">
				
				<h2>
					<span id="product_name_main" class="product_name_main">
						<?php echo $dong_sp['Name']?>
					</span>
				</h2>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div id="main_img" class="main_img">
						<img id="main_image" title="" alt="1c"
						src="http://localhost/ass2/images/<?php echo $dong_sp['mainImage']?>" class="img-responsive center-block" style="width:400px; height: 500px">
					</div>
					<div id="small_imgs" class="small_imgs col-md-12">
						<div class="col-md-4">
							<a title="" 
							onmouseover="changeImage(this, 'main_image', 'http://localhost/ass2/images/<?php echo $dong_sp['subImages']?>', 400, 600, '', '1a');"
							onclick="changeImage(this, 'main_image', 'http://localhost/ass2/images/<?php echo $dong_sp['subImages']?>', 400, 600, '', '1a');" href="../images/1a.jpg"
							target="_blank">
								<img class="child_image" title="" alt="1a" src="http://localhost/ass2/images/<?php echo $dong_sp['subImages']?>"
								style="width:110px; height: 130px
								">
							</a>
						</div>
						<div class="col-md-4">
							<a title="" 
							onmouseover="changeImage(this, 'main_image', 'http://localhost/ass2/images/<?php echo $dong_sp['subImages']?>', 400, 600, '', '1c');"
							onclick="changeImage(this, 'main_image', 'http://localhost/ass2/images/<?php echo $dong_sp['subImages']?>', 400, 600, '', '1a');" href="images/1c.jpg"
							target="_blank">
								<img class="child_image" title="" alt="1c" src="http://localhost/ass2/images/<?php echo $dong_sp['subImages']?>"
								style="width:110px; height: 130px
								">
							</a>
						</div>
						<div class="col-md-4">
							<a title="" 
							onmouseover="changeImage(this, 'main_image', 'http://localhost/ass2/images/<?php echo $dong_sp['subImages']?>', 400, 600, '', '1b');" 
							onclick="changeImage(this, 'main_image', 'http://localhost/ass2/images/<?php echo $dong_sp['subImages']?>', 400, 600, '', '1a');" href="images/1b.jpg" 
							target="_blank">
							<img class="child_image" title="" alt="1b" src="http://localhost/ass2/images/<?php echo $dong_sp['subImages']?>"
								style="width:110px; height: 130px
								">
							</a>
						</div>
					</div> 
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="product_stock">	
						<table>
							<tbody>
								<tr>
									<td>
										<label class="price"><?php echo number_format($dong_sp['Price'])?>đ</label>
									</td>
								</tr>
								<tr>
									<td>
										<div class="input-group">
											<input type="number" id="number_of_products" min="1" class="number_products" value="1" >
										</div>
										
										<a href="?id=<?php echo $id?>&giohang=<?php echo $id?>"><button type="button" class="btn btn-primary btn-md" style="background-color: #c2d44e; border-color: white;">Thêm vào giỏ hàng</button> 
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div id="product_bottom_part" class="product_bottom_part">
						<div id="product_description_main" class="product_description_main">
							<p><?php echo $dong_sp['description']?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	
	<?php include('footer.php');?>
</body>
</html>