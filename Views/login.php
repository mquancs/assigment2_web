
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/styleHome.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/style_intro.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">

<?php 
include('header.php');
include('navUser.php');
?>


    
    <div class="row">
        <div class="col-sm-12 mt30 mb30">
            <h2 class="text-center no-margin mb20-xs" style="color: #c2d44e; font-weight: bold;">Login to Your Account</h2>

        </div>
        <div class="text-center no-margin mb20-xs" id="errMsg" style="color: red">
            <?php if(!empty($_SESSION['errMsg'])) { echo $_SESSION['errMsg']; } ?>
        </div>
        <?php unset($_SESSION['errMsg']); ?>
        <div class="col-sm-12">
            <form class="form-horizontal" action="../Controller/loginValidator.php" method="POST">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="username">Username: </label>
                         <div class="col-sm-4 ">
                            <input class="form-control checkout-form-border" id="username"
                               name="username" placeholder="Username" type="text">
                        </div>    
                    </div>
                       
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="password">Password:</label>
                        <div class="col-sm-4">
                            <input class="form-control checkout-form-border" id="password" name="password"
                                   placeholder="password " type="password">
                        </div>
                    </div>

                    <input class="btn btn-lg btn-success col-sm-1 col-sm-offset-5" type="submit" value="Login">
                </fieldset>
            </form>
        </div>
    </div>
<?php include('footer.php');?>
</div>
</body>
</html>