<?php
    require '../Controller/BlogController.php';

    $blogC= new BlogController;
    if(isset($_GET['id'])) {
        $object = $blogC->getById($_GET['id']);
    }
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Blog</title>
	
	<link rel="stylesheet" type="text/css" href="../style.css">
	<link rel="stylesheet" type="text/css" href="../css/styleHome.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	
	<?php
		include('header.php');
		include('navUser.php');
	?>

	<?php
    if(isset($_SESSION['username'])){
    	if($_SESSION['username'] == "admin"){
     ?>

    <div class="admin-buttons">
		<a class="admin-button btn btn-danger" href="../Controller/Blog_route.php?id=<?=htmlspecialchars($object->id)?>&action=delete" 
		onclick="ConfirmDelete()"> 
				Delete
		</a>

		<a class="admin-button btn btn-info" href="blogEditPost.php?id=<?=htmlspecialchars($object->id)?>">
				Edit	
		</a>
	</div>

	<?php }} ?>
	<div class="detail">
		<div class="row-img">
			<div class="col-md-12">
				<article class="article-blog">	
					
					<div class="post-title">
						<h2>
							<?php
								echo $object->title;
							?>
						</h2>
					</div>
					
					<p>
						<?php
							echo $object->content;
						?>
					</p>
					
					<div class="blog-image-main">
						<h3>#1</h3>
						<div class="image-blog-detail">
							<?php
								echo '<img src="'.$object->image.'" alt="" class="img-responsive center-block">';
							?>
						</div>
					</div>
					
				</article>
				
			</div>
		</div>
	</div>
	<!-- <div class="blog-cmts"> -->
	<div class="fb-comments" data-href="http://localhost/assignment2/Views/blogDetail.php?id=<?=htmlspecialchars($object->id)?>" data-numposts="3" data-width="100%"></div>
	<!-- </div> -->

	<<?php include('footer.php');?>
	
    <script>
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '1387053781316642',
	      xfbml      : true,
	      version    : 'v2.9'
	    });
	    FB.AppEvents.logPageView();
	  };

	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "//connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1387053781316642";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
    
</body>
</html>

