<!DOCTYPE html>
<html lang="vn">
<head>
    <title>Assignment 1</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../ass2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../ass2/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../ass2/css/styleHome.css">
    <link rel="stylesheet" type="text/css" href="../ass2/css/style.css">
    <link rel="stylesheet" type="text/css" href="../ass2/css/style_intro.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php
include("$_SERVER[DOCUMENT_ROOT]/ass2/Controller/displayProduct.php");
?>

<div class="container-fluid ">
    <div class=" hidden-xs hidden-sm" id="slide_bar"><br>
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

          <div class="item active">
            <img src="http://shopquahandmade.com/wp-content/uploads/2016/05/DCHT-cover-facebook-847x300.jpg" alt="Chania" width="555" height="350">
            <div class="carousel-caption">
              <h3>Đồ dùng học tập</h3>
            </div>
          </div>

          <div class="item">
            <img src="http://shop10k.vn/media/catalog/category/Banner_danh_m_c-DCHT.jpg" alt="Chania" width="555" height="350">
            <div class="carousel-caption">
              <h3>Quà tặng handmade</h3>
            </div>
          </div>
      
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
    
    <div class="container-fluid ">
    <div class="row">
    <?php
    $last_catalog = null;
    while($dong_catalogs=mysql_fetch_array($q)){
        if($dong_catalogs['catalog_ID'] != $last_catalog){
            $last_catalog = $dong_catalogs['catalog_ID'];
    ?>  
        <div class="col-md-12  ">
            <h3>
                 <?php echo $dong_catalogs['catalog_Name']; ?>          
            </h3>
        </div>
        <?php
        }
        ?>
        <div class="col-md-3">
            <div class="col-md-12  mb30">
                <div class="product-container">
                <a href="../../ass2/Views/productDetail.php?id=<?php echo $dong_catalogs['product_ID'] ?>">
                    <img class="img-responsive full-width" src="../../ass2/images/<?php echo $dong_catalogs['mainImage']?>" alt="...">
                </a>
                </div>
                <div class="content-box-description">
                    <a href="../../ass2/Views/productDetail.php?id=<?php echo $dong_catalogs['product_ID']?>"><h2> <?php echo $dong_catalogs['Name']?> </h2></a>
                                        <!-- <p>Phasellus ac ullamcorper augue. Cras eu sem ut elit pharetra tempor in et velit.</p> -->
                                        <?php if(isset($_SESSION['username'])){?>
                                        <a href="?giohang=<?php echo $dong_catalogs['product_ID'] ?>" class="btn btn-bordered btn-rw">
                                            Add to Cart
                                        </a>
                                        <?php }else{?>
                                            <a  class="btn btn-bordered btn-rw">
                                            Login to buy
                                        </a>
                                        <?php }?>
                                        <span class="shop-product-price"><em><span style="text-decoration:line-through; color: red"><small><?php echo number_format(round($dong_catalogs['Price']/(1-$dong_catalogs['Percent']/100), 0))?>đ</small></span></em> <?php echo number_format($dong_catalogs['Price'])?>đ</span>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
    </div>
    
</div>
    
</body>
</html>