<?php
	require '../Controller/BlogController.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Blog</title>
	
	<link rel="stylesheet" type="text/css" href="../css/styleHome.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body class="body-blog">
	
    <?php
		include('header.php');
		if(isset($_SESSION['username'])){
			if($_SESSION['username'] == "admin"){
				include('navAdmin.php');
			}else{
				include('navUser.php');
			}
		}else{
			include('navUser.php');
		}
	?>
    <?php
    if(isset($_SESSION['username'])){
    	if($_SESSION['username'] == "admin"){
     ?>
    <div class="admin-buttons">
			<a class="admin-button btn btn-info" href="blogAddPost.php">
				 <span class="glyphicon glyphicon-plus"></span>Add Post

			</a>
	</div>
	<?php } }?>
    <form action="" method="post">
		<div class="detail">
				<div class="col-md-12">
				<dic class="row">
					<?php
					$selection = new BlogController;
					$result = $selection->getAll();

					if($result) {
						while($row = $result->fetch_assoc()) {
					?>		
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<article class="item">
								
									<div class="intro-image">
										<a href="blogDetail.php?id=<?=htmlspecialchars($row['ID'])?>">
											<?php
											echo '<img src="'.$row['Images'].'" alt=""  class="img-responsive center-block">';
											?>
										</a>
									</div>

									<div class="text-item">
										<div class="post-title">
											<h2>
												<a class="link-tile" href="blog_detail.html">
														<?php
														echo $row['Title'];
														?>
												</a>
											</h2>
										</div>

										<div class="post-content">	
											<p>
												<?php
												echo $row['Content'];
												?>
											</p>
										</div>
										<p class="readmore">
											<a class="btn btn-default" href="blogDetail.php?id=<?=htmlspecialchars($row['ID'])?>">
												Read more ...	
											</a>
										</p>
									</div>
									<hr>
									<div class="item-footer" >
										<div class="share-buttons" style="margin-top: 3px; margin-left: 5px; ">
											<!-- Facebook -->
											<div class="fb-share-button" data-href="http://localhost/assignment2/Views/blogDetail.php?id=<?=htmlspecialchars($row['ID'])?>" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flocalhost%2Fassignment2%2FViews%2FblogMain.php&amp;src=sdkpreparse">Share</a></div>
										</div>	
										<div class="num-of-cmt">
											<span class="glyphicon glyphicon-comment"></span>
											  <span class="fb-comments-count" data-href="/post-link">
											  	<fb:comments-count href="http://localhost/assignment2/Views/blogDetail.php?id=<?=htmlspecialchars($row['ID'])?>"/></fb:comments-count>
											  </span>
											</a>
										</div>

									</div>
								</article>
							</div>
					<?php
						}	
					}
					?>
				</div>
			</div>
		</div>
	</form>

	<?php include('footer.php');?>

    <script>
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '1387053781316642',
	      xfbml      : true,
	      version    : 'v2.9'
	    });
	    FB.AppEvents.logPageView();
	  };

	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "//connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1387053781316642";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>

