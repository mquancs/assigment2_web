
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/styleHome.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/style_intro.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function validate(){
            var oldPassword = document.getElementById("oldPassword").value;
            var newpassword = document.getElementById("newpassword").value;
            var newpassword1 = document.getElementById("newpassword1").value;
            if(oldPassword == ""){
                alert("Please input oldPassword!");
            }
            else if(newpassword == ""){
                alert("Please input newpassword!");
            }
            else if(newpassword1 == ""){
                alert("Please input Re_enter newpassword!");
            }
            else if(newpassword != newpassword1){
                alert("New Password and Re_enter New Password must be same!");
                return;
            }
            else{
                document.getElementById("form").action="../Controller/changePasswordValidator.php";
                document.getElementById("form").submit();
            }

        }
    </script>
</head>
<body>
<div class="container-fluid">

<?php 
include('header.php');
include('navUser.php');
?>

<?php 
    include_once("../Model/user.php");
    $model = new user();
    $infors = $model->getUserInfor($_SESSION['username']);
    $infor = mysql_fetch_array($infors);
?>
    
    <div class="row">
        <div class="col-sm-12 mt30 mb30">
            <h2 class="text-center no-margin mb20-xs" style="color: #c2d44e; font-weight: bold;">CHANGE INFORMATIONS</h2>

        </div>
        <div class="text-center no-margin mb20-xs" id="sucMsg" style="color: blue">
            <?php if(!empty($_SESSION['sucMsg'])) { echo $_SESSION['sucMsg']; } ?>
        </div>
        <?php unset($_SESSION['sucMsg']); ?>
        <div class="col-sm-12">
            <form class="form-horizontal" id="form" action="../Controller/changeInforValidator.php" method="POST">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="phone">Phone: </label>
                         <div class="col-sm-4 ">
                            <input class="form-control checkout-form-border" id="phone"
                               name="oldPassword"  type="text" value="<?php echo $infor['Phone']; ?> ">
                        </div>    
                    </div>
                       
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="email">Email:</label>
                        <div class="col-sm-4">
                            <input class="form-control checkout-form-border" id="email" name="email"
                                    type="email" value="<?php echo $infor['Email']; ?> ">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="address">Address</label>
                        <div class="col-sm-4">
                            <input class="form-control checkout-form-border" id="address" name="address"
                                    type="text" value="<?php echo $infor['Address']; ?> ">
                        </div>
                    </div>

                    <div class="col-sm-8 col-sm-offset-4">
                        <div class="Finalize">
                            <button type="submit"  class="col-sm-4 btn btn-primary"
                                    style="font-weight: bold; margin-bottom: 20px;"> Change
                            </button>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>
    </div>
<?php include('footer.php');?>
</div>
</body>
</html>