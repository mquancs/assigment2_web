<!DOCTYPE html>
<html lang="vn">
<head>
    <title>Assignment 1</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/styleHome.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
            #addTable{
                display: none;
            }
            
        </style>
    <script type="text/javascript">
        $(document).ready(function(){
                
                $("button#addbtn").click(function(){
                    $("#addTable").toggle();
                });
                $("button#add_button").click(function() {
                     $.ajax({
                        url: '../../ass2/Controller/addCatalogController.php',
                        type: 'POST',
                        data: { 
                                name: $('input#name').val()
                                },
                        success: function(response) {
                            $('table#resultTable tbody').html(response);
                        }
                    });
                    $("#addTable").toggle();
                });

            });
        
    </script>
</head>
<body>
<?php  include('../Views/header.php'); include('../Views/navAdmin.php');  ?>
<div id = "content">
<div class="container-fluid row">
        <div id="order-detail " >
            <button class = "btn btn-primary" id="addbtn">Add more !</button>
            <div id = "addTable">
            <table class = "table" >   
                <tr>
                    <td>Name</td>
                    <td>
                    <input type="text" id="name" name="name" >
                    </td>
                </tr> 
                
                
                
                <br>
            </table>
            <button class="btn btn-primary" id = "add_button">Submit</button>
            </div>
            <h2 class="order-title">Thông tin giỏ hàng :</h2>
            <div class="table-responsive">
            <table class="table " id="resultTable">
                <thead>
                <tr>
                    <th>ID </th>
                    <th>NAME</th>
                    <th>DISCOUNTID</th>
                    
                    <th>Tac vu</th>
                </tr>
                </thead><!-- /table header -->
                <tbody>
                <?php 
                while ($row = mysql_fetch_array($infors)) {
                ?>
                <tr>
                    <td>
                        <?php echo $row{'catalog_ID'} ?>

                    </td>
                    <td class="cart-price"><?php echo $row{'catalog_Name'} ?></td>
                    
                    <td><?php echo $row{'Discount_ID'} ?></td>
                    <td >
                    <button class = "btn btn-success" id = "edit<?php echo $row{'catalog_ID'} ?>">Edit</button>
                    <button class = "btn btn-danger" id = "delete<?php echo $row{'catalog_ID'} ?>">Delete</button>
                        
                    </td>
                    <script>
                        $("button#delete<?php echo $row{'catalog_ID'} ?>").click(function() {
                             $.ajax({
                                url: "../../ass2/Controller/deleteCatalogController.php",
                                type: "POST",
                                data: { id: "<?php echo $row{'catalog_ID'} ?>"
                                       },
                                success: function(response) {
                                    $("table#resultTable tbody").html(response);
                                }
                            });
                        });
                        $("button#edit<?php echo $row{'catalog_ID'} ?>").click(function() {
                            idEdit = "<?php echo $row{'catalog_ID'} ?>";
                            $("tr#rowedit<?php echo $row{'catalog_ID'} ?>").css("visibility","visible");
                            
                        });
                        
                    </script>
                </tr>
                <tr id="rowedit<?php echo $row{'catalog_ID'} ?>" style="visibility: hidden">
                    <td><input type="text" name="idEdit" value="<?php echo $row{'catalog_ID'} ?>" readonly>
                    </td>
                    <td class="cart-price"><input type="text" name="nameEdit<?php echo $row{'catalog_ID'} ?>" id="nameEdit<?php echo $row{'catalog_ID'} ?>" value = "<?php echo $row{'catalog_Name'} ?>">
                    </td>
                    <td><input type="text" name="discountEdit<?php echo $row{'catalog_ID'} ?>" id="discountEdit<?php echo $row{'catalog_ID'} ?>" value="<?php echo $row{'Discount_ID'} ?>" readonly> 
                    </td>
                    
                    <td >
                        <button class = "btn btn-primary" id = "editbtn<?php echo $row{'catalog_ID'} ?>">Submit</button>
                        <button class = "btn btn-primary" id = "editcancel<?php echo $row{'catalog_ID'} ?>">Cancel</button>   
                    </td>
                    <script type="text/javascript">
                        $("button#editcancel<?php echo $row{'catalog_ID'} ?>").click(function() {
                            $("tr#rowedit<?php echo $row{'catalog_ID'} ?>").css("visibility","hidden");   
                        });
                        $("button#editbtn<?php echo $row{'catalog_ID'} ?>").click(function(){
                    $.ajax({
                        url: "../../ass2/Controller/editCatalogController.php",
                        type: "POST",
                        data: {
                                id : <?php echo $row{'catalog_ID'} ?>,
                                name: $("input#nameEdit<?php echo $row{'catalog_ID'} ?>").val()},
                        success: function(response) {
                            $("table#resultTable tbody").html(response);
                        }
                    });
                    $("button#editbtn").click(function(){
                    
                });
                });
                    </script>
                </tr>
                <?php } ?>

                
                </tbody><!-- /table body -->
            </table>
            </div>            
    </div>
</div>
</div>
<?php include('../Views/footer.php'); ?>

</body>
</html>