<div class="container-fluid" id="footer">
        <div class="row">
            <div class="col-md-3">
                <h3>Thông tin</h3>
                <ul>
                    <li><span class="glyphicon glyphicon-ok"></span> &nbsp; HANDMADE SHOP</li>
                    <li><h3>Địa chỉ: </h3></li>
                    <li class="arrow">268 - Lý Thường Kiệt - Phường 14 - Quận 10 - TPHCM</li>
                </ul>
            </div>
            <div class="col-md-3">
                <h3>Liên hệ</h3>
                <ul>
                    <li class="phone">&nbsp;0977 667 668 - 0988 115 226</li>
                    <li class="mail">&nbsp;webdisigner@ygmail.com</li>
                    <li class="skype">&nbsp;Skype</li>
                    <li class="facebook">&nbsp;Facebook</li>
                    <li class="twitter">&nbsp;Twiter</li>
                </ul>
            </div>
        </div>
    </div>

    <div id="copyright"><span>©2017 Assignment 1 – Web Interface Design</span></div>