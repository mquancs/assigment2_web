<?php  ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>Sign Up</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/styleHome.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/style_intro.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    	function validate(){
    		
    		var username = document.getElementById("username").value;
    		var password = document.getElementById("password").value;
    		var password1 = document.getElementById("reInputPassword").value;
    		var phone = document.getElementById("phone").value;
    		var address = document.getElementById("address").value;
    		if(username == ""){
    			alert("Please input username!");
    		}
    		else if(password == ""){
    			alert("Please input password!");
    		}
    		else if(password1 == ""){
    			alert("Please input Re_enter Password!");
    		}
    		else if(phone == ""){
    			alert("Please input phone!");
    		}
    		else if(address == ""){
    			alert("Please input address!");
    		}
    		else if(password != password1){
    			alert("Password and Re_enter Password must be same!");
    			return;
    		}
    		else{
    			document.getElementById("form").action="../Controller/signUpValidator.php";
    			document.getElementById("form").submit();
    		}

    	}
    </script>
</head>
<body>
<div class="container-fluid">
    <?php 
include('header.php');
include('navUser.php');
?>


    
    <div class="row">
        <div class="col-sm-12 mt30 mb30">
            <h2 class="text-center no-margin mb20-xs" style="color: #c2d44e; font-weight: bold;">Sign Up</h2>

        </div>
        <div class="text-center no-margin mb20-xs" id="errMsg" style="color: red">
            <?php if(!empty($_SESSION['errMsg'])) { echo $_SESSION['errMsg']; } ?>
        </div>
        <?php unset($_SESSION['errMsg']); ?>
        <div class="col-sm-12">
            <form class="form-horizontal" method="POST"  id = "form">
                <fieldset>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="username">Username *</label>
                        <div class="col-sm-4">
                            <input class="form-control checkout-form-border" id="username"
                                   name="username" placeholder="xuantung1025" type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="password">Password *</label>
                        <div class="col-sm-4">
                            <input class="form-control checkout-form-border" id="password" name="password"
                                   placeholder=" " >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="reInputPassword">Re_enter Password *</label>
                        <div class="col-sm-4">
                            <input class="form-control checkout-form-border" id="reInputPassword" name="reInputPassword"
                                   placeholder=" " >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="phone">Phone Number *</label>
                        <div class="col-sm-4">
                            <input class="form-control checkout-form-border" id="phone" name="phone"
                                   placeholder="0973501825" type="number">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="address">Address* </label>
                        <div class="col-sm-4">
                            <input class="form-control checkout-form-border" id="address" name="address"
                                   placeholder="268 Ly Thuong Kiet">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="email">Email </label>
                        <div class="col-sm-4">
                            <input class="form-control checkout-form-border" id="email" name="email"
                                   placeholder="abc@gmail.com" type="email">
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-sm-offset-4">
                        <div class="Finalize">
                            <button onclick="validate()"  class="col-sm-4 btn btn-primary"
                                    style="font-weight: bold; margin-bottom: 20px;"> Create your PU SHOP account
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <?php 
	include('footer.php');
	?>
</div>
</body>
</html>