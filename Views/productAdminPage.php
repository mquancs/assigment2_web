<!DOCTYPE html>
<html lang="vn">
<head>
    <title>Assignment 1</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/styleHome.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <style>
            #addTable{
                display: none;
            }
            
        </style>
    <script type="text/javascript">
        $(document).ready(function(){
                
                $("button#addbtn").click(function(){
                    $("#addTable").toggle();
                });
                $("button#add_button").click(function() {
                     $.ajax({
                        url: 'http://localhost/ass2/Controller/addProductController.php',
                        type: 'POST',
                        data: { catalogId: $('input#catalogId').val(), 
                                name: $('input#name').val(),
                                price: $('input#price').val(),
                                mainImage: $('input#mainImage').val(),
                                subImages: $('input#subImages').val()},
                        success: function(response) {
                            $('table#resultTable tbody').html(response);
                        }
                    });
                    $("#addTable").toggle();
                });

            });
        
    </script>
</head>
<body>
<?php  include('../Views/header.php'); include('../Views/navAdmin.php');  ?>
<div id = "content">
<div class="container-fluid row">
        <div id="order-detail " >
            <button class = "btn btn-primary" id="addbtn">Add more !</button>
            <div id = "addTable">
            <table class = "table" >   
                <tr>
                    <td>Catalog Id</td>
                    <td>
                    <input type="text" id="catalogId" name="catalogId" value="<?php echo $catalogId ?>" readonly>
                    </td>
                </tr> 
                <tr>
                    <td>Name</td>
                    <td>
                    <input type="text" id="name" name ="name" placeholder="name">
                    </td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td>
                    <input type="text" id="price" name = "price" placeholder="price">
                    </td>
                </tr>
                <tr>
                    <td>mainImage</td>
                    <td>
                    <input type="text" id="mainImage" name = "mainImage" placeholder="image">
                    </td>
                </tr>
                <tr>
                    <td>subImages</td>
                    <td>
                    <input type="text" id="subImages" name = "subImages" placeholder="images">
                    </td>
                </tr>
                
                <br>
            </table>
            <button class="btn btn-primary" id = "add_button">Submit</button>
            </div>
            <h2 class="order-title">Thông tin giỏ hàng :</h2>
            <div class="table-responsive">
            <table class="table " id="resultTable">
                <thead>
                <tr>
                    <th>ID SAN PHAM</th>
                    <th>NAME</th>
                    <th>Price</th>
                    <th>mainImage</th>
                    <th>subImages</th>
                    <th>Tac vu</th>
                </tr>
                </thead><!-- /table header -->
                <tbody>
                <?php 
                while ($row = mysql_fetch_array($infors)) {
                ?>
                <tr>
                    <td>
                        <?php echo $row{'product_ID'} ?>

                    </td>
                    <td class="cart-price"><?php echo $row{'Name'} ?></td>
                    <td>
                        <?php echo $row{'Price'} ?>
                    </td>
                    <td><?php echo $row{'mainImage'} ?></td>
                    <td><?php echo $row{'subImages'} ?></td>
                    <td >
                    <button class = "btn btn-success" id = "edit<?php echo $row{'product_ID'} ?>">Edit</button>
                    <button class = "btn btn-danger" id = "delete<?php echo $row{'product_ID'} ?>">Delete</button>
                        
                    </td>
                    <script>
                        $("button#delete<?php echo $row{'product_ID'} ?>").click(function() {
                             $.ajax({
                                url: "http://localhost/ass2/Controller/deleteProductController.php",
                                type: "POST",
                                data: { id: "<?php echo $row{'product_ID'} ?>",
                                        catalogId :  <?php echo $catalogId ?>},
                                success: function(response) {
                                    $("table#resultTable tbody").html(response);
                                }
                            });
                        });
                        $("button#edit<?php echo $row{'product_ID'} ?>").click(function() {
                            idEdit = "<?php echo $row{'product_ID'} ?>";
                            $("tr#rowedit<?php echo $row{'product_ID'} ?>").css("visibility","visible");
                            
                        });
                        
                    </script>
                </tr>
                <tr id="rowedit<?php echo $row{'product_ID'} ?>" style="visibility: hidden">
                    <td><input type="text" name="idEdit" value="<?php echo $row{'product_ID'} ?>" readonly>
                    </td>
                    <td class="cart-price"><input type="text" name="nameEdit<?php echo $row{'product_ID'} ?>" id="nameEdit<?php echo $row{'product_ID'} ?>" value = "<?php echo $row{'Name'} ?>">
                    </td>
                    <td><input type="text" name="priceEdit<?php echo $row{'product_ID'} ?>" id="priceEdit<?php echo $row{'product_ID'} ?>" value="<?php echo $row{'Price'} ?>"> 
                    </td>
                    <td><input type="text" name="mainImageEdit<?php echo $row{'product_ID'} ?>" id="mainImageEdit<?php echo $row{'product_ID'} ?>" value="<?php echo $row{'mainImage'} ?>">
                    </td>
                    <td><input type="text" name="subImagesEdit<?php echo $row{'product_ID'} ?>" id="subImagesEdit<?php echo $row{'product_ID'} ?>" value="<?php echo $row{'subImages'} ?>">
                    </td>
                    <td >
                        <button class = "btn btn-primary" id = "editbtn<?php echo $row{'product_ID'} ?>">Submit</button>
                        <button class = "btn btn-primary" id = "editcancel<?php echo $row{'product_ID'} ?>">Cancel</button>   
                    </td>
                    <script type="text/javascript">
                        $("button#editcancel<?php echo $row{'product_ID'} ?>").click(function() {
                            $("tr#rowedit<?php echo $row{'product_ID'} ?>").css("visibility","hidden");   
                        });
                        $("button#editbtn<?php echo $row{'product_ID'} ?>").click(function(){
                    $.ajax({
                        url: "http://localhost/ass2/Controller/editProductController.php",
                        type: "POST",
                        data: {
                                id : <?php echo $row{'product_ID'} ?>,
                                catalogId : <?php echo $catalogId ?>,
                                name: $("input#nameEdit<?php echo $row{'product_ID'} ?>").val(), 
                                price: $("input#priceEdit<?php echo $row{'product_ID'} ?>").val(),
                                mainImage: $("input#mainImageEdit<?php echo $row{'product_ID'} ?>").val(),
                                subImages: $("input#subImagesEdit<?php echo $row{'product_ID'} ?>").val()},
                        success: function(response) {
                            $("table#resultTable tbody").html(response);
                        }
                    });
                    $("button#editbtn").click(function(){
                    
                });
                });
                    </script>
                </tr>
                <?php } ?>

                
                </tbody><!-- /table body -->
            </table>
            </div>            
    </div>
</div>
</div>
<?php include('../Views/footer.php'); ?>

</body>
</html>