<!DOCTYPE html>
<html lang="vn">
<head>
    <title>Assignment 1</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/styleHome.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
            #addTable{
                display: none;
            }
            
        </style>
    <script type="text/javascript">
        $(document).ready(function(){
                
                $("button#addbtn").click(function(){
                    $("#addTable").toggle();
                });
                $("button#add_button").click(function() {
                     $.ajax({
                        url: '../../ass2/Controller/addPromotionController.php',
                        type: 'POST',
                        data: { 
                                percent: $('input#percent').val(),
                                startDate: $('input#startDate').val(),
                                endDate: $('input#endDate').val()},
                        success: function(response) {
                            $('table#resultTable tbody').html(response);
                        }
                    });
                    $("#addTable").toggle();
                });

            });
        
    </script>
</head>
<body>
<?php  include('../Views/header.php'); include('../Views/navAdmin.php');  ?>
<div id = "content">
<div class="container-fluid row">
        <div id="order-detail " >
            <button class = "btn btn-primary" id="addbtn">Add more !</button>
            <div id = "addTable">
            <table class = "table" >   
                <tr>
                    <td>Percent</td>
                    <td>
                    <input type="text" id="percent" name="percent" >
                    </td>
                </tr> 
                <tr>
                    <td>Start Date</td>
                    <td><input type="date" id="startDate" name = "startDate" ></td>
                </tr>
                <tr>
                    <td>End Date</td>
                    <td>
                    <input type="date" id="endDate" name = "endDate" >
                    </td>
                </tr>
                
                
                
                <br>
            </table>
            <button class="btn btn-primary" id = "add_button">Submit</button>
            </div>
            <h2 class="order-title">Thông tin giỏ hàng :</h2>
            <div class="table-responsive">
            <table class="table " id="resultTable">
                <thead>
                <tr>
                    <th>ID </th>
                    <th>Percent</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Tac vu</th>
                </tr>
                </thead><!-- /table header -->
                <tbody>
                <?php 
                while ($row = mysql_fetch_array($infors)) {
                ?>
                <tr>
                    <td>
                        <?php echo $row{'discount_ID'} ?>

                    </td>
                    <td class="cart-price"><?php echo $row{'Percent'} ?></td>
                    
                    <td><input type="date" name="startdate" value="<?php echo $row{'StartDate'} ?>" ></td>
                    <td><input type="date" name="enddate" value="<?php echo $row{'EndDate'} ?>" ></td>
                    <td >
                    <button class = "btn btn-success" id = "edit<?php echo $row{'discount_ID'} ?>">Edit</button>
                      
                    </td>
                    <script>
                        
                        $("button#edit<?php echo $row{'discount_ID'} ?>").click(function() {
                            idEdit = "<?php echo $row{'discount_ID'} ?>";
                            $("tr#rowedit<?php echo $row{'discount_ID'} ?>").css("visibility","visible");
                            
                        });
                        
                    </script>
                </tr>
                <tr id="rowedit<?php echo $row{'discount_ID'} ?>" style="visibility: hidden">
                    <td><input type="text" name="idEdit" value="<?php echo $row{'discount_ID'} ?>" readonly>
                    </td>
                    <td class="cart-price"><input type="text" name="percentEdit<?php echo $row{'discount_ID'} ?>" id="percentEdit<?php echo $row{'discount_ID'} ?>" value = "<?php echo $row{'Percent'} ?>">
                    </td>
                    <td><input type="date" name="startDateEdit<?php echo $row{'discount_ID'} ?>" id="startDateEdit<?php echo $row{'discount_ID'} ?>" value="<?php echo $row{'StartDate'} ?>"> 
                    </td>
                    <td><input type="date" name="endDateEdit<?php echo $row{'discount_ID'} ?>" id="endDateEdit<?php echo $row{'discount_ID'} ?>" value="<?php echo $row{'EndDate'} ?>"> 
                    </td>
                    
                    <td >
                        <button class = "btn btn-primary" id = "editbtn<?php echo $row{'discount_ID'} ?>">Submit</button>
                        <button class = "btn btn-primary" id = "editcancel<?php echo $row{'discount_ID'} ?>">Cancel</button>   
                    </td>
                    <script type="text/javascript">
                        $("button#editcancel<?php echo $row{'discount_ID'} ?>").click(function() {
                            $("tr#rowedit<?php echo $row{'discount_ID'} ?>").css("visibility","hidden");   
                        });
                        $("button#editbtn<?php echo $row{'discount_ID'} ?>").click(function(){
                    $.ajax({
                        url: "../../ass2/Controller/editPromotionController.php",
                        type: "POST",
                        data: {
                                id : <?php echo $row{'discount_ID'} ?>,
                                percent: $("input#percentEdit<?php echo $row{'discount_ID'} ?>").val(),
                                endDate: $("input#endDateEdit<?php echo $row{'discount_ID'} ?>").val(), 
                                startDate: $("input#startDateEdit<?php echo $row{'discount_ID'} ?>").val()},
                        success: function(response) {
                            $("table#resultTable tbody").html(response);
                        }
                    });
                    $("button#editbtn").click(function(){
                    
                });
                });
                    </script>
                </tr>
                <?php } ?>

                
                </tbody><!-- /table body -->
            </table>
            </div>            
    </div>
</div>
</div>
<?php include('../Views/footer.php'); ?>

</body>
</html>