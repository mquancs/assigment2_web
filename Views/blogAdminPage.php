<!DOCTYPE html>
<html lang="en">
<head>
    <title>Assignment 1</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/styleHome.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style_intro.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
            #addTable{
                display: none;
            }
            
        </style>
    <script type="text/javascript">
        $(document).ready(function(){
                
                $("button#addbtn").click(function(){
                    $("#addTable").toggle();
                });
                $("button#add_button").click(function() {
                     $.ajax({
                        url: '../../assignment2_web/Controller/addBlogController.php',
                        type: 'POST',
                        data: { 
                                title: $('input#title').val(),
                                content: $('input#content').val(),
                                date: $('input#date').val(),
                                author: $('input#author').val()},
                        success: function(response) {
                            $('table#resultTable tbody').html(response);
                        }
                    });
                    $("#addTable").toggle();
                });

            });
        
    </script>
</head>
<body>
<?php  include('../Views/header.php'); include('../Views/navAdmin.php');  ?>
<div id = "content">
<div class="container-fluid row">
        <div id="order-detail " >
            <button class = "btn btn-primary" id="addbtn">Add more car!</button>
            <div id = "addTable">
            <table class = "table" >   
                <tr>
                    <td>Title</td>
                    <td>
                    <input type="text" id="title" name="title" >
                    </td>
                </tr> 
                <tr>
                    <td>Content</td>
                    <td>
                    <input type="text" id="content" name ="content" placeholder="content">
                    </td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>
                    <input type="date" id="date" name = "date" >
                    </td>
                </tr>
                <tr>
                    <td>Author</td>
                    <td>
                    <input type="text" id="author" name = "author" value="admin">
                    </td>
                </tr>
                
                
                <br>
            </table>
            <button class="btn btn-primary" id = "add_button">Submit</button>
            </div>
            <h2 class="order-title">Thông tin giỏ hàng :</h2>
            <div class="table-responsive">
            <table class="table " id="resultTable">
                <thead>
                <tr>
                    <th>ID </th>
                    <th>TITLE</th>
                    <th>CONTENT</th>
                    <th>DATE</th>
                    <th>AUTHOR</th>
                    <th>Tac vu</th>
                </tr>
                </thead><!-- /table header -->
                <tbody>
                <?php 
                while ($row = mysql_fetch_array($infors)) {
                ?>
                <tr>
                    <td>
                        <?php echo $row{'ID'} ?>

                    </td>
                    <td class="cart-price"><?php echo $row{'Title'} ?></td>
                    <td>
                        <?php echo $row{'Content'} ?>
                    </td>
                    <td><?php echo $row{'PostingDate'} ?></td>
                    <td><?php echo $row{'Author'} ?></td>
                    <td >
                    <button class = "btn btn-success" id = "edit<?php echo $row{'ID'} ?>">Edit</button>
                    <button class = "btn btn-danger" id = "delete<?php echo $row{'ID'} ?>">Delete</button>
                        
                    </td>
                    <script>
                        $("button#delete<?php echo $row{'ID'} ?>").click(function() {
                             $.ajax({
                                url: "../../assignment2_web/Controller/deleteBlogController.php",
                                type: "POST",
                                data: { id: "<?php echo $row{'ID'} ?>"
                                       },
                                success: function(response) {
                                    $("table#resultTable tbody").html(response);
                                }
                            });
                        });
                        $("button#edit<?php echo $row{'ID'} ?>").click(function() {
                            idEdit = "<?php echo $row{'ID'} ?>";
                            $("tr#rowedit<?php echo $row{'ID'} ?>").css("visibility","visible");
                            
                        });
                        
                    </script>
                </tr>
                <tr id="rowedit<?php echo $row{'ID'} ?>" style="visibility: hidden">
                    <td><input type="text" name="idEdit" value="<?php echo $row{'ID'} ?>" readonly>
                    </td>
                    <td class="cart-price"><input type="text" name="titleEdit<?php echo $row{'ID'} ?>" id="titleEdit<?php echo $row{'ID'} ?>" value = "<?php echo $row{'Title'} ?>">
                    </td>
                    <td><input type="text" name="contentEdit<?php echo $row{'ID'} ?>" id="contentEdit<?php echo $row{'ID'} ?>" value="<?php echo $row{'Content'} ?>"> 
                    </td>
                    <td><input type="text" name="mainImageEdit<?php echo $row{'ID'} ?>" id="mainImageEdit<?php echo $row{'ID'} ?>" value="<?php echo $row{'PostingDate'} ?>" readonly>
                    </td>
                    <td><input type="text" name="subImagesEdit<?php echo $row{'ID'} ?>" id="subImagesEdit<?php echo $row{'ID'} ?>" value="<?php echo $row{'Author'} ?> " readonly>
                    </td>
                    <td >
                        <button class = "btn btn-primary" id = "editbtn<?php echo $row{'ID'} ?>">Submit</button>
                        <button class = "btn btn-primary" id = "editcancel<?php echo $row{'ID'} ?>">Cancel</button>   
                    </td>
                    <script type="text/javascript">
                        $("button#editcancel<?php echo $row{'ID'} ?>").click(function() {
                            $("tr#rowedit<?php echo $row{'ID'} ?>").css("visibility","hidden");   
                        });
                        $("button#editbtn<?php echo $row{'ID'} ?>").click(function(){
                    $.ajax({
                        url: "../../assignment2_web/Controller/editBlogController.php",
                        type: "POST",
                        data: {
                                id : <?php echo $row{'ID'} ?>,
                                title: $("input#titleEdit<?php echo $row{'ID'} ?>").val(), 
                                content: $("input#contentEdit<?php echo $row{'ID'} ?>").val()},
                        success: function(response) {
                            $("table#resultTable tbody").html(response);
                        }
                    });
                    $("button#editbtn").click(function(){
                    
                });
                });
                    </script>
                </tr>
                <?php } ?>

                
                </tbody><!-- /table body -->
            </table>
            </div>            
    </div>
</div>
</div>
<?php include('../Views/footer.php'); ?>

</body>
</html>