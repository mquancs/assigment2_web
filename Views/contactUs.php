<?php
	require '../Controller/BlogController.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Blog</title>
	
	<link rel="stylesheet" type="text/css" href="../css/styleHome.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body class="body-blog">
	
    <?php
		include('header.php');
		include('navUser.php');
		
	?>
	<?php 
        if(isset($_SESSION['sucMsg'])){
    ?>
    <div class="container-fluid row">
    <div>
        <div class="content" style="text-align: center;">
            <h1 style="color: blue; font-weight: bold;">Cảm ơn sự phản hồi từ quý khách.</h1>
        </div>
    </div>
    </div>
    <?php 
        unset($_SESSION['sucMsg']);
        }

    ?>
    <div class="container-fluid">
    <form action="../Controller/contactUsController.php" method="post">
		<div class="row">
        <div class="col-md-6 col-xs-12">

            <div class="heading mb20"><h4>Send us a Message</h4></div>
            <p class="mb20">Aenean sodales erat sit amet quam tincidunt tincidunt. Sed aliquet <b>mauris sit amet
                ullamcorper</b> sodales.</p>
            <form role="form">
                <div class="form-group">
                    <input type="text" name="name" placeholder="Name" class="form-control" id="InputName">
                </div>
                <div class="form-group">
                    <input type="email" name="email" placeholder="Email Address" class="form-control" id="InputEmail1">
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="message" placeholder="Message" id="InputMessage" rows="7"></textarea>
                </div>
                <button type="submit" class="btn btn-rw btn-primary" style="margin-bottom: 20px;">Submit</button>
            </form>
        </div>

        <div class="col-md-6 col-xs-12">
            <div class="content-box content-box-primary mb30">
                <p class="phone-icon"> &#9743;</p>
                <h2 class="text-white no-margin">702-423-1315</h2>
            </div>
            <div class="panel panel-primary no-margin">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="ion-android-system-home"></span> Information</h3>
                </div>
                <div class="panel-body">
                    <address class="no-margin">
                        <strong>Raleway, Inc.</strong><br>
                        702 Goose Down Ave, Suite 1<br>
                        Malibu, CA 93217<br>
                        <abbr title="Phone">P:</abbr> (123) 456-7890 <br>
                        Mail: <a href="pages-contact-1.html#">support@raleway.com</a>
                    </address>
                </div>
            </div>
        </div>
    </div>
	</form>
	</div>
	<?php include('footer.php');?>

    

</body>
</html>

