<?php 
    $connect = mysql_connect("localhost","root","") or die("error");
    $select_db = mysql_select_db("ass2", $connect) or die("error");
    mysql_query("SET NAMES 'utf8' ");
?>
<!doctype html>
<html>
<head>
    <title>chi tiet don hang</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/styleHome.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/style_intro.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="../css/styleHome.css">
    <script type="text/javascript">
        function myFunction() {
            var pass1 = document.getElementById("newPass").value;
            var pass2 = document.getElementById("confirmNewPass").value;
            if (pass1 != pass2) {
                document.getElementById('message').innerHTML = "please confirm the new password ";
            }
        }
    </script>
    <style type="text/css">
        .review-col {
            width: 140px;
        }

        .table.cart-table > thead > tr > th {
            background-color: #c2d44e;
            color: #CC9752;
            padding: 12px 15px;
            font-size: 16px;
        }

        .total {
            color: red;
            font-weight: bold;
            font-size: 24px;
        }

        .cost-total {
            color: red;
            font-size: 16px;
        }

        .order-title {
            color: #003c71;
            font-size: 24px;
            font-weight: bold;
        }
    </style>
</head>

<body>
<div class="container-fluid">
    <?php
        include('header.php');
        include('navUser.php');
        $tongtien = 0;
        
    ?>
    <?php 
        if(isset($_SESSION['sucMsg'])){
    ?>
    <div class="container-fluid row">
    <div>
        <div class="content" style="text-align: center;">
            <h1 style="color: blue; font-weight: bold;">Cảm ơn sự ủng hộ của bạn.</h1>
            <h1  style="color: blue; font-weight: bold;" >Chúng tôi sẽ liên lạc với bạn sớm.</h1>
            <div><a href="http://localhost/ass2/">Quay về trang chủ.</a></div>
        </div>

    </div>
    </div>
    <?php 
        unset($_SESSION['sucMsg']);
        }else{

    ?>
    <div class="container-fluid row">
        <div id="order-detail " >
            <h2 class="order-title">Thông tin giỏ hàng :</h2>
            <form method="POST" action="../Controller/handleOrder.php">
            <fieldset>
            <div class="table-responsive">
            <table class="table cart-table table-striped">
                <thead>
                <tr>
                    <th>Sản phẩm</th>
                    <th>Giá</th>
                    <th>Số lượng</th>
                    <th>Khuyến mãi</th>
                    <th class="total-title">Tổng tiền</th>
                </tr>
                </thead><!-- /table header -->
                <tbody>
                <?php $i = 0;
                foreach ($_SESSION as $name => $value) {

                    if($value > 0 ){
                        if(substr($name, 0, 5) == 'cart_'){
                            $id = substr($name, 5, strlen($name-5));
                            $sql = mysql_query("SELECT * FROM  products WHERE product_ID = ".$id);

                            if(!$sql){
                                echo "error".'<br/>';
                            }else{
                                
                                while($row = mysql_fetch_array($sql)){
                                    $i += 1;
                                    $tien = $row['Price']*$value;
                                    $tongtien += $tien;?>
                <tr>
                    <td>
                        <a href="shop-single-product.html" class="product-name" name="productname"><?php echo $row['Name']?></a>
                        <input type="hidden" name="productId<?php echo $i?>"  value=" <?php echo $row['product_ID'];?> " > 
                    </td>
                    <td class="cart-price" ><?php echo number_format($row['Price'])?>đ</td>
                    <td type="text" class="form-control cart-quantity">
                        <input type="numeric" name="quantity<?php echo $i?>" value="<?php echo $value; ?>"  style="border: 0px;" readonly>
                    </td>
                    <td>0</td>
                    <td class="cart-total" >
                    <input type="text" name="price<?php echo $i?>" value="<?php echo ($tien); ?>" style="border: 0px;" readonly>
                    </td>
                    
                </tr>
                <?php 
                                }
                            }
                        }
                    }
                }
                ?>
                <tr>
                    <td class="total" colspan="4">Total</td>
                    <td class="cost-total"><input type="text" style="border: 0px;" name="totalPrice"  value="<?php echo number_format($tongtien) ?>" readonly></td>
                </tr>
                </tbody><!-- /table body -->
            </table>
            </div>
            <button type="submit" class="btn btn-primary" style="font-weight: bold; margin-bottom: 20px;">Thanh toán</button>
            </fieldset>
            </form>
        </div>
    </div>
    <?php }?>
    <?php   
        include('footer.php');
    ?>
</div>
</body>
</html>
