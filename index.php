<!DOCTYPE html>
<html lang="en">
<head>
    <title>Assignment 1</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../ass2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../ass2/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../ass2/css/styleHome.css">
    <link rel="stylesheet" type="text/css" href="../ass2/css/style.css">
    <link rel="stylesheet" type="text/css" href="../ass2/css/style_intro.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid ">
<?php 
include('Views/header.php');
if (isset($_SESSION['username']) && $_SESSION['username'] == "admin") {
    header("location: ../../ass2/Controller/transactionValidateController.php");
}
include('Views/navUser.php');
include('Views/home.php');
include('Views/footer.php');?>
</div>
</body>
</html>