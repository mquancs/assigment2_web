-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2017 at 03:57 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sales`
--

-- --------------------------------------------------------

--
-- Table structure for table `catalogs`
--

CREATE TABLE IF NOT EXISTS `catalogs` (
`catalog_ID` int(6) unsigned NOT NULL,
  `catalog_Name` varchar(64) CHARACTER SET utf8 NOT NULL,
  `Discount_ID` int(6) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `catalogs`
--

INSERT INTO `catalogs` (`catalog_ID`, `catalog_Name`, `Discount_ID`) VALUES
(2, 'Đồ dùng học tập', 1),
(3, 'Quà tặng valentine', 2),
(4, 'Sản phẩm hot', 3);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
`ID` int(6) unsigned NOT NULL,
  `User_ID` int(6) NOT NULL,
  `Post_ID` int(6) NOT NULL,
  `Content` text,
  `CommentingDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `contactUs` (
  `ID` int(6) UNSIGNED NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `message` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contactUs`
--
ALTER TABLE `contactUs`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contactUs`
--
ALTER TABLE `contactUs`
  MODIFY `ID` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
`discount_ID` int(6) unsigned NOT NULL,
  `Percent` int(2) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`discount_ID`, `Percent`, `StartDate`, `EndDate`) VALUES
(1, 20, '2017-04-17', '2017-05-31'),
(2, 15, '2017-04-17', '2017-06-10'),
(3, 10, '2017-04-17', '2017-06-10');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`ID` int(6) unsigned NOT NULL,
`transaction_ID` int(6) NOT NULL,
  `User_ID` int(6) NOT NULL,
  `Product_ID` int(6) NOT NULL,
  `Quantity` int(6) NOT NULL,
  `orderPrice` decimal(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`ID` int(6) unsigned NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Content` text,
  `AmountOfViews` int(12) DEFAULT NULL,
  `PostingDate` date DEFAULT NULL,
  `Author` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`product_ID` int(6) unsigned NOT NULL,
  `Catalog_ID` int(6) NOT NULL,
  `Name` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `Price` double NOT NULL,
  `mainImage` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `subImages` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_ID`, `Catalog_ID`, `Name`, `Price`, `mainImage`, `description`, `subImages`) VALUES
(1, 2, 'Tập Flower', 30000, 'hoctap1.jpg', 'Tập Flower Tập Flower Tập Flower Tập Flower Tập Flower Tập Flower Tập Flower Tập Flower Tập Flower Tập Flower Tập Flower Tập Flower', 'hoctap1.jpg'),
(2, 2, 'Quạt tay Touch Me', 25000, 'hoctap2.jpg', 'Quạt tay Touch Me Quạt tay Touch Me Quạt tay Touch Me Quạt tay Touch Me Quạt tay Touch Me Quạt tay Touch Me Quạt tay Touch Me', 'hoctap2.jpg'),
(3, 2, 'Trứng ngộ nghĩnh', 15000, 'hoctap3.jpg', 'Trứng ngộ nghĩnh Trứng ngộ nghĩnh Trứng ngộ nghĩnh Trứng ngộ nghĩnh Trứng ngộ nghĩnh Trứng ngộ nghĩnh ', 'hoctap3.jpg'),
(4, 2, 'Bút chì nhiều màu', 27900, 'hoctap4.jpg', 'Bút chì nhiều màu Bút chì nhiều màu Bút chì nhiều màu Bút chì nhiều màu Bút chì nhiều màu Bút chì nhiều màu', 'hoctap4.jpg'),
(5, 3, 'Dây chuyền', 69000, 'valentine1.jpg', 'Dây chuyền Dây chuyền Dây chuyền Dây chuyền Dây chuyền Dây chuyền Dây chuyền Dây chuyền', 'valentine1.jpg'),
(6, 3, 'Vòng tay', 69900, 'valentine2.jpg', 'Vòng tay Vòng tay Vòng tay Vòng tay Vòng tay Vòng tay Vòng tay Vòng tay Vòng tay', 'valentine2.jpg'),
(7, 3, 'Vòng tay innox', 79000, 'valentine3.jpg', 'Vòng tay innox Vòng tay innox Vòng tay innox Vòng tay innox Vòng tay innox Vòng tay innox ', 'valentine3.jpg'),
(8, 3, 'Vòng tay vải len', 49900, 'valentine4.jpg', 'Vòng tay vải len Vòng tay vải len Vòng tay vải len Vòng tay vải len Vòng tay vải len', 'valentine4.jpg'),
(9, 4, 'Son môi', 69000, 'sphot1.jpg', 'Son môi Son môi Son môi Son môi Son môi Son môi Son môi Son môi Son môi Son môi Son môi', 'sphot1.jpg'),
(10, 4, 'Gấu handmade', 99900, 'sphot2.jpg', 'Gấu handmade Gấu handmade Gấu handmade Gấu handmade Gấu handmade Gấu handmade Gấu handmade Gấu handmade', 'sphot2.jpg'),
(11, 4, 'Kính mát cho nam', 109900, 'sphot3.jpg', 'Kính mát cho nam Kính mát cho nam Kính mát cho nam Kính mát cho nam Kính mát cho nam Kính mát cho nam', 'sphot3.jpg'),
(12, 4, 'Balo happy', 150000, 'sphot4.jpg', 'Balo happy Balo happy Balo happy Balo happy Balo happy Balo happy Balo happy Balo happy', 'sphot4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
`ID` int(6) NOT NULL,
  `User_ID` int(6) NOT NULL,
  `TotalPrice` decimal(12,2) NOT NULL,
  `TransactionDate` date NOT NULL,
  `Status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`ID` int(6) NOT NULL,
  `Name` varchar(128) NOT NULL,
  `Password` varchar(128) NOT NULL,
  `Phone` int(11) DEFAULT NULL,
  `Email` varchar(128) NOT NULL,
  `Position` varchar(40) NOT NULL,
  `Address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=100001 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `Name`, `Password`, `Phone`, `Email`, `Position`, `Address`) VALUES
(100000, 'Admind', '202cb962ac59075b964b07152d234b70', 123456789, 'admin@gmail.com', 'admin', 'Ly Thuong Kiet');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `catalogs`
--
ALTER TABLE `catalogs`
 ADD PRIMARY KEY (`catalog_ID`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
 ADD PRIMARY KEY (`discount_ID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`product_ID`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `catalogs`
--
ALTER TABLE `catalogs`
MODIFY `catalog_ID` int(6) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
MODIFY `ID` int(6) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
MODIFY `discount_ID` int(6) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `ID` int(6) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
MODIFY `ID` int(6) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `product_ID` int(6) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
MODIFY `ID` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `ID` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=100001;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
